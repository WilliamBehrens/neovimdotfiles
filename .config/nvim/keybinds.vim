" leader = space
let mapleader=" "

" Fzf config
let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8} }
let $FZF_DEFAULT_OPTS='--reverse'
nmap <leader>sf :Files <CR>
nmap <leader>sa :Ag <CR>
nmap <leader>sr :Rg <CR>
nmap <C-f> :FZF <CR>

" Window controls
nmap <leader>h :wincmd h <CR>
nmap <leader>j :wincmd j <CR>
nmap <leader>k :wincmd k <CR>
nmap <leader>l :wincmd l <CR>
nmap <leader>vs :vsplit <CR>
nmap <leader>s :split <CR>


" Panel controls
nmap <leader>f :NERDTree <CR>
nmap <leader>t :wincmd s <BAR> :wincmd j <BAR> :resize 20 <BAR> :term <CR>
nmap <C-e> :NERDTree <BAR> :wincmd l <BAR> :split <BAR> :wincmd j <BAR> :resize 20 <BAR> :term <CR>
nmap <leader>ut :UndotreeShow <CR>
tnoremap <Esc> <C-\><C-n>

" Ctrl remaps
map! <C-q> :q <CR>
map! <C-x> <ESC>

" Get rid of some terrible bindings
map s :w <CR>
map q <C>
map r <C>

" System commands
nmap <A-f> :!touch <SPACE>
nmap <A-d> :!mkdir <SPACE>

" Tab controls
map <C-i> :tabprevious <CR>
nmap <leader><tab> :tabn <CR>
nmap <C-T> :tabnew <BAR> :NERDTree <CR>

" debugger
let g:nvimgdb_config_override = {
  \ 'key_next': 'n',
  \ 'key_step': 's',
  \ 'key_finish': 'f',
  \ 'key_continue': 'c',
  \ 'key_until': 'u',
  \ 'key_breakpoint': 'b',
  \ }
  "\ 'set_tkeymaps': "NvimGdbNoTKeymaps",

"emmet keybinds TODO: look up docs
let g:user_emmet_mode='n'
let g:user_emmet_leader_key=','


" lsp
nnoremap <silent> D :lua vim.lsp.buf.definition()<CR>
nnoremap <silent> H :lua vim.lsp.buf.hover()<CR>
nnoremap <silent> I :lua vim.lsp.buf.implementation()<CR>
"nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
"nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
"nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
"nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
"nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
"nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
