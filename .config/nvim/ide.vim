"""""""""""""""""""""""""""
"   Neovim LSP settings   "
"""""""""""""""""""""""""""

" general settings
set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

" enable lsp here: require'lspconfig'.<server>.setup{}
lua << EOF
    require'lspconfig'.clangd.setup{ on_attach=require'completion'.on_attach }

    require'lspconfig'.kotlin_language_server.setup{ on_attach=require'completion'.on_attach }

    require'lspconfig'.nimls.setup{ on_attach=require'completion'.on_attach }

    --Enable (broadcasting) snippet capability for completion
    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities.textDocument.completion.completionItem.snippetSupport = true

    require'lspconfig'.html.setup {
        capabilities = capabilities,
        on_attach=require'completion'.on_attach,
        filetypes = { "html", "hbs" }
    }

    require'lspconfig'.cssls.setup{
        capabilities = capabilities,
        on_attach=require'completion'.on_attach,
        filetypes = { "css", "less" }
    }

EOF

"""""""""""""""""""""""""""
"   Ale config (linter)   "
"""""""""""""""""""""""""""

" linters, this adds some autocomplete and can conflict with regualer lsp at times
let g:ale_linters = {
\   'c': ['clangd', 'clang', 'clang-tidy', 'clang-check'],
\   'cpp': ['clangd', 'clang', 'cppcheck', 'clang-tidy', 'clang-check'],
\   'json': ['jq'],
\   'html': ['htmlhint', 'tidy'],
\   'dockerfile': ['hadolint'],
\   'kotlin': [ 'ktlint', 'languageserver'],
\   'git': ['gitlint'],
\   'less': ['lessc'],
\   'nim': ['nimlsp', 'nimcheck'],
\   'ruby': ['solargraph', 'ruby']
\}

" fixers, still don't know how to use this
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'cpp': ['clang-format'],
\   'kotlin': ['ktlint'],
\   'nim': ['nimpretty']
\}

" general settings
let g:ale_linters_explicit = 1
let g:ale_fix_on_save = 1
"let g:ale_completion_enabled = 1
let g:ale_completion_autoimport = 1
let g:ale_set_highlights = 1
let g:ale_lint_on_enter = 1
let g:ale_set_quickfix = 1
let g:ale_fix_on_save = 1
let g:ale_sign_error = '✖✖'
let g:ale_sign_warning = '⚠⚠'
let g:airline#extensions#ale#enabled = 1
highlight clear ALEErrorSign guifg=Red
highlight clear ALEWarningSign guifg=Yellow
