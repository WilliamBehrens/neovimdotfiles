" basic settings
syntax on
set hidden
filetype indent on
filetype plugin indent on
set tabstop=4 " show existing tab with 4 spaces width
set shiftwidth=4 " when indenting with '>', use 4 spaces width
set expandtab " On pressing tab, insert 4 spaces
set smartindent
set cindent
set autoindent
set number
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.cache/nvim/undodir
set undofile
set termguicolors
set incsearch
set encoding=UTF-8
set guifont=Go\ Mono\ Nerd\ Font\ complete\ Mono\ 11

" Auto indenting
inoremap {<CR> {<CR>}<C-o>O

source ~/.config/nvim/plugins.vim
source ~/.config/nvim/keybinds.vim
source ~/.config/nvim/ide.vim
source ~/.config/nvim/ThemerVim.vim

" Disables line numbers in terminal
autocmd TermOpen * setlocal nonumber norelativenumber

" rst document viewer
let g:instant_rst_browser = 'librewolf'
let g:instant_rst_port = '7420'
"let g:instant_rst_localhost_only = 1

" Nerd tree file manager config
let g:NERDTreeGitStatusConcealBrackets = 1
let NERDTreeShowHidden = 1

" airline status bar config
let g:airline#extensions#nerdtree_statusline = 1
