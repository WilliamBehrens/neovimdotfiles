
call plug#begin('~/.config/nvim/plugged')

    " I give up, if I give up again just use lsp from coc
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    " Git integration
    Plug 'tpope/vim-fugitive'

    " Auto close pairs
    Plug 'jiangmiao/auto-pairs'

    " Light weight status bar
    Plug 'vim-airline/vim-airline'

    " Advanced search functions
    Plug 'junegunn/fzf', {'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'

    " Linter and formater
    Plug 'w0rp/ale'

    "https://github.com/sirver/ultisnips
    "Plug 'sirver/ultisnips'

    " Better undo functionality
	Plug 'mbbill/undotree'

    " Icons in vim
    Plug 'ryanoasis/vim-devicons'

    " Better file manager
    Plug 'preservim/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'PhilRunninger/nerdtree-visual-selection'

    " LSP
    Plug 'neovim/nvim-lspconfig'
    Plug 'nvim-lua/completion-nvim'

    " Extra language support
    "Plug 'hallzy/gravity.vim'
    "Plug 'Olical/conjure'
    Plug 'baabelfish/nvim-nim'
    Plug 'lluchs/vim-wren'
    Plug 'jdonaldson/vaxe'
    Plug 'udalov/kotlin-vim'
    Plug 'alvan/vim-closetag'
    Plug 'mustache/vim-mustache-handlebars'
    Plug 'groenewege/vim-less'

    " ABC completion
    Plug 'Shougo/deoplete.nvim'

call plug#end()
